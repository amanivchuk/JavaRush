package com.manivchukCash.comand;

import com.manivchukCash.ConsoleHelper;
import com.manivchukCash.CurrencyManipulator;
import com.manivchukCash.CurrencyManipulatorFactory;

import java.util.Collection;
public class InfoCommand implements Command
{
    @Override
    public void execute() {
        Collection<CurrencyManipulator> manipulatorMap = CurrencyManipulatorFactory.getAllCurrencyManipulator();
        if(manipulatorMap.isEmpty()){
            ConsoleHelper.writeMessage("Nothing in Mape!");
        }else{
            int count = 0;
            for(CurrencyManipulator manipulator : manipulatorMap){
                if(manipulator.hasMoney() && manipulator.getTotalAmount() > 0){
                    ConsoleHelper.writeMessage(manipulator.getCurrencyCode() + " - " + manipulator.getTotalAmount());
                    count++;
                }
            }
            if(count == 0)
                ConsoleHelper.writeMessage("No money available.");
        }
    }
}
