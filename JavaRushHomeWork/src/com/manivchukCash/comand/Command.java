package com.manivchukCash.comand;

public interface Command {
    void execute();
}
