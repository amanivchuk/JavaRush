package com.manivchukCash.comand;

import com.manivchukCash.ConsoleHelper;
import com.manivchukCash.CurrencyManipulator;
import com.manivchukCash.CurrencyManipulatorFactory;

public class DepositCommand implements Command
{
    @Override
    public void execute() {
        String code = ConsoleHelper.askCurrencyCode();
        CurrencyManipulator cur = CurrencyManipulatorFactory.getManipulatorByCurrencyCode(code);
        String[] arr = ConsoleHelper.getValidTwoDigits(code);
        cur.addAmount(Integer.parseInt(arr[0]), Integer.parseInt(arr[1]));

    }
}
