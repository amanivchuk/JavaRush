package com.manivchukCash.comand;

import com.manivchukCash.Operation;

import java.util.HashMap;
import java.util.Map;

public class CommandExecutor {
    private static Map<Operation, Command> map = null;
    static{
        map = new HashMap<>();
        map.put(Operation.INFO, new InfoCommand());
        map.put(Operation.DEPOSIT, new DepositCommand());
        map.put(Operation.WITHDRAW, new WithdrawCommand());
        map.put(Operation.EXIT, new ExitCommand());
    }
    public static final void execute(Operation operation){
/*        for(Map.Entry<Operation, Command> m : map.entrySet()){
            if(m.getKey().equals(operation)){
                m.getValue().execute();
            }
        }*/
        map.get(operation).execute();
    }
}
