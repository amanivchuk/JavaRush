package com.manivchukCash;

import java.util.*;
public class CurrencyManipulatorFactory {
    private static ArrayList<CurrencyManipulator> listCurrencyManipulator = new ArrayList<>();
    private CurrencyManipulatorFactory() {}

    public static ArrayList<CurrencyManipulator> getAllCurrencyManipulator() {
        return listCurrencyManipulator;
    }

    public static CurrencyManipulator getManipulatorByCurrencyCode(String currencyCode){
        CurrencyManipulator manipulator = null;
        for(CurrencyManipulator i : listCurrencyManipulator){
            if(i.getCurrencyCode().equals(currencyCode)){
                manipulator = i; break;
            }
        }
        if(manipulator == null){
            manipulator = new CurrencyManipulator(currencyCode);
            listCurrencyManipulator.add(manipulator);
        }
        return manipulator;
    }
}
