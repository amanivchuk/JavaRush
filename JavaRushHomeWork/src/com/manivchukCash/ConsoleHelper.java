package com.manivchukCash;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleHelper {
   private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    public static void writeMessage(String message){
        System.out.println(message);
    }
    public static String readString(){
        String s = "";
        try{
            s = reader.readLine();
        }catch (IOException e){}
        return s;
    }

    public static String askCurrencyCode(){
        writeMessage("Enter code!");
        String code = readString();
        while(code.length() != 3){
            writeMessage("Wrong!");
            code = readString();
        }
        String str = code.toUpperCase();
        return str;
    }
    public static String[] getValidTwoDigits(String currencyCode){
        writeMessage("Enter nominal and count banknot!");
        int nominal;
        int banknota;
        String[] array;
        while(true){
            String s = readString();
            array = s.split(" ");
            try{
                nominal = Integer.parseInt(array[0]);
                banknota = Integer.parseInt(array[1]);
                if(nominal <= 0 || banknota <= 0 || array.length > 2){
                    writeMessage("Wrong!");
                    continue;
                }else break;
            }catch (Exception e){
                writeMessage("Wrong!");
            }
        }
        return array;
    }
    public static Operation askOperation(){
        writeMessage("Enter operation!");
        int operation;
        while(true){
            try{
                operation = Integer.parseInt(readString());
                return Operation.getAllowableOperationByOrdinal(operation);
            }catch (IllegalArgumentException e){
                writeMessage("Wrong number");
                askOperation();
            }
        }
    }
}
