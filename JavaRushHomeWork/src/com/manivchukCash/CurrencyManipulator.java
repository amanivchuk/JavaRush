package com.manivchukCash;

import java.util.HashMap;
import java.util.Map;

public class CurrencyManipulator {
    private String currencyCode;
    Map<Integer, Integer> denominations = new HashMap<>();

    public CurrencyManipulator(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }
    public void addAmount(int denomination, int count){
        if(denominations.containsKey(denomination))
            denominations.put(denomination,denominations.get(denomination) + count);
        else
            denominations.put(denomination,count);
    }

    public boolean hasMoney() {
        if(denominations.isEmpty())
            return false;
        else return true;
    }

    public int getTotalAmount() {
        int total = 0;
        for(Map.Entry<Integer,Integer> m : denominations.entrySet()){
            int key = m.getKey();
            int value = m.getValue();
            int sum = key * value;
            total += sum;
        }
        return total;
    }
}
