package com.manivchuk.test.task;

import com.manivchuk.test.task.kitchen.Cook;

import java.io.IOException;

public class Restaurant {
    public static void main(String[] args) throws IOException
    {
        Tablet tablet = new Tablet(5);
        Cook cook = new Cook("Amigo");
        tablet.addObserver(cook);
        tablet.createOrder();
    }
}
