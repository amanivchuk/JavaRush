package com.manivchuk.test.task.kitchen;

import java.util.Arrays;

public enum Dish {
    Fish,
    Steak,
    Soup,
    Juice,
    Water;

    public static String allDishesToString(){
        StringBuilder builder = new StringBuilder(Arrays.toString(Dish.values()));
        builder.delete(0, 1);
        builder.delete(builder.length()-1,builder.length());
        return builder.toString();
    }
}
