package com.manivchuk.test.task.kitchen;

import com.manivchuk.test.task.ConsoleHelper;

import java.util.Observable;
import java.util.Observer;

public class Cook implements Observer{
    private String name;

    public Cook(String name){
        this.name = name;
    }

    public String toString(){
        return this.name;
    }

    @Override
    public void update(Observable o, Object arg) {
        ConsoleHelper.writeMessage("Start cooking - " + arg);
    }
}
