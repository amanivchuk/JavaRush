package com.manivchuk.test.task.kitchen;

import com.manivchuk.test.task.ConsoleHelper;
import com.manivchuk.test.task.Tablet;

import java.io.IOException;
import java.util.*;

public class Order {
    private Tablet tablet;
    private List<Dish> dishes;

    public Order(Tablet tablet) throws IOException {
        this.tablet = tablet;
        dishes = ConsoleHelper.getAllDishesForOrder();

    }
    public String toString(){
        StringBuilder builder = new StringBuilder();
        if(dishes.isEmpty()){
            return "";
        }else{
            builder.append("Your order: ").append(dishes).append(" of ").append(this.tablet);
            return builder.toString();
        }
    }
}
