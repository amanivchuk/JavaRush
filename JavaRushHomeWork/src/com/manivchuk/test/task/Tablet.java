package com.manivchuk.test.task;

import com.manivchuk.test.task.kitchen.Order;

import java.io.IOException;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Tablet extends Observable{
    final int number;
    private static Logger logger = Logger.getLogger(Tablet.class.getName());

    public Tablet(int number){
        this.number = number;
    }
    public void createOrder() throws IOException {
        try{
            Order order = new Order(this);
            ConsoleHelper.writeMessage(order.toString());
            setChanged();
            notifyObservers(order);
        }catch (IOException e){
            logger.log(Level.SEVERE,"Console is unavailable.");
        }
    }
    public String toString(){
        return "Tablet{" + this.number + "}";
    }
}
