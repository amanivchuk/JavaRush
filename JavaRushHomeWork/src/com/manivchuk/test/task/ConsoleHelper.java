package com.manivchuk.test.task;

import com.manivchuk.test.task.kitchen.Dish;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class ConsoleHelper {
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    public static void writeMessage(String message){
        System.out.println(message);
    }
    public static String readString() throws IOException
    {
        return reader.readLine();
    }
    public static List<Dish> getAllDishesForOrder() throws IOException {
        List<Dish> listDishes = new ArrayList<>();
        Dish[] arrayDishes = Dish.values();
        writeMessage(Dish.allDishesToString());
        while (true) {
            writeMessage("Choose dishes:");
            String str = readString();
            if(str.toLowerCase().equals("exit"))
                break;
            boolean flag = false;

            for(int i = 0; i < arrayDishes.length; i++){
                if(str.equalsIgnoreCase(arrayDishes[i].toString())){
                    listDishes.add(Dish.valueOf(str));
                    flag = true;
                    break;
                }
            }
            if(!flag){
                writeMessage("No Dish");
            }
        }
        return listDishes;
    }
}
