package com.javarush.test.level19.lesson10.home08;

/* Перевертыши
1 Считать с консоли имя файла.
2 Для каждой строки в файле:
2.1 переставить все символы в обратном порядке
2.2 вывести на экран
3 Закрыть потоки. Не использовать try-with-resources

Пример тела входного файла:
я - программист.
Амиго

Пример результата:
.тсиммаргорп - я
огимА
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader file_1 = new BufferedReader(new FileReader(reader.readLine()));
        //BufferedWriter file_2 = new BufferedWriter(new FileWriter(reader.readLine()));
        ArrayList<String> list = new ArrayList<String>();
        while(file_1.ready()){
                list.add(file_1.readLine());
        }
        file_1.close();
        //file_2.close();
        reader.close();

        ArrayList<String> result = new ArrayList<String>();
        for(String s : list){
            result.add(new StringBuilder().append(s).reverse().toString());
        }
        for(String s : result){
            System.out.println(s);
        }
    }
}
