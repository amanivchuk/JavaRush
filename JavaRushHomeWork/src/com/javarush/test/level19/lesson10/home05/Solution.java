package com.javarush.test.level19.lesson10.home05;

/* Слова с цифрами
В метод main первым параметром приходит имя файла1, вторым - файла2.
Файл1 содержит строки со слов, разделенные пробелом.
Записать через пробел в Файл2 все слова, которые содержат цифры, например, а1 или abc3d
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        //BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader file_1 = new BufferedReader(new FileReader(args[0]));
        BufferedWriter file_2 = new BufferedWriter(new FileWriter(args[1]));
        while(file_1.ready()){
            String[] str = file_1.readLine().split(" ");
            for(String s : str){
                if(s.matches(".*\\d+.*")){
                    file_2.write(s + " ");
                }
                file_2.write("\n");
            }
        }
        //reader.close();
        file_1.close();
        file_2.close();
    }
}
