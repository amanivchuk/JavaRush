package com.javarush.test.level19.lesson10.home02;

/* Самый богатый
В метод main первым параметром приходит имя файла.
В этом файле каждая строка имеет следующий вид:
имя значение
где [имя] - String, [значение] - double. [имя] и [значение] разделены пробелом

Для каждого имени посчитать сумму всех его значений
Вывести в консоль имена, у которых максимальная сумма
Имена разделять пробелом либо выводить с новой строки
Закрыть потоки. Не использовать try-with-resources

Пример входного файла:
Петров 0.501
Иванов 1.35
Петров 0.85

Пример вывода:
Петров
*/

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader file = new BufferedReader(new FileReader(args[0]));
        TreeMap<String, Double> map = new TreeMap<String, Double>();
        String name;
        double value;
        String[] mas;
        while((name = file.readLine()) != null){
            mas = name.split(" ");
            if(map.containsKey(mas[0])){
                value = map.get(mas[0]);
                map.put(mas[0], Double.parseDouble(mas[1]) + value);
            } else{
                map.put(mas[0], Double.parseDouble(mas[1]));
            }
        }
        file.close();
        Double maxValue = Collections.max(map.values());
        for(Map.Entry<String, Double> m : map.entrySet()){
            if(Objects.equals(maxValue, m.getValue())){
                System.out.println(m.getKey());
            }
        }
    }
}
