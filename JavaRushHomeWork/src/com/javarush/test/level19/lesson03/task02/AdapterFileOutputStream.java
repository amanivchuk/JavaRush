package com.javarush.test.level19.lesson03.task02;

/* Адаптер
Используйте класс AdapterFileOutputStream, чтобы адаптировать FileOutputStream к новому интерфейсу AmigoStringWriter
*/

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class AdapterFileOutputStream implements AmigoStringWriter {
    private FileOutputStream fileOutputStream;

    public AdapterFileOutputStream(FileOutputStream fileOutputStream)
    {
        this.fileOutputStream = fileOutputStream;
    }

    public void write(int b) throws IOException
    {
        fileOutputStream.write(b);
    }

    public void write(byte[] b) throws IOException
    {
        fileOutputStream.write(b);
    }

    public void write(byte[] b, int off, int len) throws IOException
    {
        fileOutputStream.write(b, off, len);
    }

    @Override
    public void close() throws IOException
    {
        fileOutputStream.close();
    }

    public FileDescriptor getFD() throws IOException
    {
        return fileOutputStream.getFD();
    }

    public FileChannel getChannel()
    {
        return fileOutputStream.getChannel();
    }

    @Override
    public void flush() throws IOException
    {
        fileOutputStream.flush();
    }

    @Override
    public void writeString(String s) throws IOException
    {
        byte[] b  = s.getBytes();
        this.fileOutputStream.write(b);
    }
}

