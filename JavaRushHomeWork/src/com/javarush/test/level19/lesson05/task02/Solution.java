package com.javarush.test.level19.lesson05.task02;

/* Считаем слово
Считать с консоли имя файла.
Файл содержит слова, разделенные знаками препинания.
Вывести в консоль количество слов "world", которые встречаются в файле.
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader reader = new BufferedReader(new FileReader(read.readLine()));
       // BufferedWriter writer = new BufferedWriter(new FileWriter(read.readLine()));
        ArrayList<String> list = new ArrayList<String>();
        String s;
        while((s = reader.readLine()) != null)
        {

            list.add(s.replaceAll("[\\p{P}]", ""));
        }
        String[] words;
        int count = 0;
        for(String a : list) {
            words = a.split(" ");
            for(String str : words){
                if(str.equalsIgnoreCase("world")){
                    count++;
                }
            }
        }
        read.close();
        reader.close();
        System.out.println(count);
    }
}
