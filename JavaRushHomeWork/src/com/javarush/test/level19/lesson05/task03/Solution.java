package com.javarush.test.level19.lesson05.task03;

/* Выделяем числа
Считать с консоли 2 имени файла.
Вывести во второй файл все числа, которые есть в первом файле.
Числа выводить через пробел.
Закрыть потоки. Не использовать try-with-resources

Пример тела файла:
12 text var2 14 8v 1

Результат:
12 14 1
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader reader = new BufferedReader(new FileReader(read.readLine()));
        BufferedWriter writer = new BufferedWriter(new FileWriter(read.readLine()));
        ArrayList<String> list = new ArrayList<String>();
        String str;
        while(reader.ready()){
            str = reader.readLine();
            String[] mas = str.split(" ");
            for(String s : mas){
                try
                {
                    int data = Integer.parseInt(s);
                    writer.write(data + " ");
                }
                catch (NumberFormatException e)
                {

                }
            }
        }

//        while((str = reader.readLine()) != null){
//            list.add(str.replaceAll("[^0-9]+", " "));
//        }
//        String[] mas;
//        for(String s : list){
//            mas = s.split(" ");
//            for(String aMas : mas){
//                //System.out.println(aMas);
//                writer.write(aMas + " ");
//            }
//        }
        read.close();
        reader.close();
        writer.close();
    }
}
