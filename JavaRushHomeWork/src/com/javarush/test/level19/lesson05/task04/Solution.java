package com.javarush.test.level19.lesson05.task04;

/* Замена знаков
Считать с консоли 2 имени файла.
Первый Файл содержит текст.
Заменить все точки "." на знак "!", вывести во второй файл.
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
        String file_1 = read.readLine();
        String file_2 = read.readLine();
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file_1)));
        PrintWriter write  = new PrintWriter(new FileOutputStream(file_2));
        ArrayList<String> arr = new ArrayList<String>();
        String s;
        while((s = reader.readLine())!= null)
        {
            arr.add(s.replaceAll("\\.","!"));
        }
        for(String as : arr){
            write.println(as);
        }
        read.close();
        reader.close();
        write.close();
    }
}
