package com.javarush.test.level19.lesson05.task05;

/* Пунктуация
Считать с консоли 2 имени файла.
Первый Файл содержит текст.
Удалить все знаки пунктуации, вывести во второй файл.
http://ru.wikipedia.org/wiki/%D0%9F%D1%83%D0%BD%D0%BA%D1%82%D1%83%D0%B0%D1%86%D0%B8%D1%8F
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader reader = new BufferedReader(new FileReader(read.readLine()));
        BufferedWriter write = new BufferedWriter(new FileWriter(read.readLine()));
        ArrayList<String> list = new ArrayList<String>();
        String s;
        while((s = reader.readLine()) != null){
            list.add(s.replaceAll("[\\p{P}]",""));
        }
        for(String str : list){
            write.write(str);
        }
        reader.close();
        read.close();
        write.close();

    }
}
