package com.javarush.test.level26.lesson10.home01;

import java.util.concurrent.BlockingQueue;

/**
 * Created by ASUS on 11.01.2016.
 */
public class Consumer implements Runnable {
    private final BlockingQueue queue;

    public Consumer(BlockingQueue queue)
    {
        this.queue = queue;
    }
    @Override
    public void run()
    {
        while (true){
            try {
                System.out.println(queue.take());
                Thread.sleep(500);
            }
            catch (InterruptedException e)
            {
                System.out.println(String.format("[%s] thread was terminated", Thread.currentThread().getName()));
            }
        }
    }
}
