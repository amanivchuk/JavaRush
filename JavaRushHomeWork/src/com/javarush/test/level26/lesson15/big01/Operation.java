package com.javarush.test.level26.lesson15.big01;

public enum Operation {
    LOGIN(0), INFO(1), DEPOSIT(2), WITHDRAW(3), EXIT(4);

    private int id;

    Operation(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }

    public static Operation getAllowableOperationByOrdinal(Integer i){
        switch (i){
            case 0 : throw new IllegalArgumentException();
            case 1 : return Operation.INFO;
            case 2 : return Operation.DEPOSIT;
            case 3 : return Operation.WITHDRAW;
            case 4 : return Operation.EXIT;
            default: throw new IllegalArgumentException();
        }
    }
}
