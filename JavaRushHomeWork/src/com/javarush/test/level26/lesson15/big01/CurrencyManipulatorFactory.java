package com.javarush.test.level26.lesson15.big01;

import java.util.ArrayList;
import java.util.Collection;

public class CurrencyManipulatorFactory {
    private static ArrayList<CurrencyManipulator> listManipulator = new ArrayList<>();

    private CurrencyManipulatorFactory() {}

    public static CurrencyManipulator getManipulatorByCurrencyCode(String currencyCode){
        CurrencyManipulator currencyManipulator = null;
        for(CurrencyManipulator manipulator : listManipulator){
            if(manipulator.getCurrencyCode().equals(currencyCode)){
                currencyManipulator = manipulator;
                break;
            }
        }
        if(currencyManipulator == null){
            currencyManipulator = new CurrencyManipulator(currencyCode);
            listManipulator.add(currencyManipulator);
        }
        return currencyManipulator;
    }
    public static Collection<CurrencyManipulator> getAllCurrencyManipulators(){
        return listManipulator;
    }
}
