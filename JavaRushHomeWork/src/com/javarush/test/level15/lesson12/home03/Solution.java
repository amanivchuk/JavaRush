package com.javarush.test.level15.lesson12.home03;

import java.math.BigDecimal;

/* ��� - ����������
1.� ������ Tree ��������� ����� info(Object s) ��� ���� ���, ����� ���������� ��� ������ info(Object s), info(Number s), info(String s).
1.1. ��������� � ������ info(Object s). ������ �� �������� ���������� ����� �������.
1.2. ��������, ��� ������ info(Number s) ��������� ����� ���� ����� "������ � 123 , ����� Number, �������� Short".
2. � ����� 2 ������ ���������� ����� info(Object s).
3. � ����� 3 ������ ���������� ����� info(Number s).
4. � ����� 4 ������ ���������� ����� info(String s).
*/

public class Solution {
    public static void main(String[] args) {
        //���� 2.
        //����� ��� Object (������� 2)
        new Tree().info(new Integer("4"));
        new Tree().info(new Short("4"));
        new Tree().info(new BigDecimal("4"));

        //���� 3.
        //����� ��� Number (������� 3)
        new Tree().info(new Integer("4"));
        new Tree().info(new Short("4"));
        new Tree().info(new BigDecimal("4"));

        //���� 4.
        //����� ��� String (������� 4)
        new Tree().info(new String("4"));
        new Tree().info(new Integer("4").toString());
        new Tree().info(new Short("4").toString());
        new Tree().info(new BigDecimal("4").toString());
    }
}
