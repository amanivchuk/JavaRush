package com.javarush.test.level16.lesson13.home06;

/* �������� �� �������
���������, ��� �������� ���������.
�� ������ � ������� CountDownRunnable ������ ���� CountUpRunnable, ������� ������� �������� � ���������� ������� - �� 1 �� number
*/

public class Solution {
    public static int number = 5;

    public static void main(String[] args) {
        new CountDownRunnable("����");
        new CountUpRunnable("�����");
        //new CountUpRunnable("�����");
        //new CountUpRunnable("������");
    }

    //Add your code below - �������� ��� ����
    public static class CountUpRunnable implements Runnable {
        private int countIndexUp = 1;
        private Thread t;
        public CountUpRunnable(String name) {
            t = new Thread(this, name);
            t.start();
            //t.join();
        }

        @Override
        public void run()
        {
            try{
                while(true){
                    System.out.println(toString());
                    ++countIndexUp;
                    if(countIndexUp >number) return;
                    Thread.sleep(500);
                }
            }catch(InterruptedException e){

            }
        }

        public String toString()
        {
            return t.getName() + ": " + countIndexUp;
        }
    }

    public static class CountDownRunnable implements Runnable {
        private int countIndexDown = Solution.number;
        private Thread t;

        public CountDownRunnable(String name) {
            t = new Thread(this, name);
            t.start();
        }

        public void run() {
            try {
                while (true) {
                    System.out.println(toString());
                    countIndexDown -= 1;
                    if (countIndexDown == 0) return;
                    Thread.sleep(500);
                }
            } catch (InterruptedException e) {
            }
        }

        public String toString() {
            return t.getName() + ": " + countIndexDown;
        }
    }
}
