package com.javarush.test.level12.lesson12.bonus02;

/* Нужно добавить в программу новую функциональность
Сделать класс Pegas(пегас) на основе класса Horse(лошадь) и интерфейса Fly(летать).
*/

public class Solution
{
    public static void main(String[] args)
    {
        print(new Beer());
        print(new Cola());
    }
    public static void print(Drink drink)
    {
        System.out.println(drink.getClass().getSimpleName());
    }
    public interface Drink
    {
        boolean isAlcoholic();
    }
    public static class Cola implements Drink
    {
        public boolean isAlcoholic()
        {
            return true;
        }
    }
    public static class Beer implements Drink
    {
        public boolean isAlcoholic()
        {
            return true;
        }
    }

}