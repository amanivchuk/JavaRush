package com.javarush.test.level30.lesson15.big01;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Server {
    private static Map<String, Connection> connectionMap = new ConcurrentHashMap<>(); //где ключом будет имя клиента, а значением - соединение с ним
    public static void sendBroadcastMessage(Message message){
        for(Map.Entry<String,Connection> map : connectionMap.entrySet()){
            try {
                map.getValue().send(message);
            }
            catch (IOException e) {
                ConsoleHelper.writeMessage("Error sending massage: " + message.toString());
            }
        }
    }

    private static class Handler extends Thread
    {
        private final Socket socket;

        Handler(Socket socket)
        {
            this.socket = socket;
        }

        public void run(){
            String errorMesage = null;
            String userName = null;
            SocketAddress socketAddress = null;

            try(Connection connection = new Connection(socket)){
                socketAddress = connection.getRemoteSocketAddress();
                errorMesage = "Error send data to remote server: " + socketAddress;
                ConsoleHelper.writeMessage("Create new connection with remote server: " + socketAddress);
                userName = serverHandshake(connection);
                sendBroadcastMessage(new Message(MessageType.USER_ADDED,userName));
                sendListOfUsers(connection,userName);
                serverMainLoop(connection,userName);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            catch (ClassNotFoundException e)
            {
                e.printStackTrace();
            }
            finally
            {
                if(userName != null){
                    connectionMap.remove(userName);
                    sendBroadcastMessage(new Message(MessageType.USER_REMOVED, userName));
                }
            }
            ConsoleHelper.writeMessage("Connection is close with remote server: " + socketAddress);
        }

        private String serverHandshake(Connection connection) throws IOException, ClassNotFoundException
        {
            while (true)
            {
                connection.send(new Message(MessageType.NAME_REQUEST));
                Message message = connection.receive();
                if (message.getType() == MessageType.USER_NAME)
                {
                    if (message.getData() != null && !message.getData().isEmpty())
                    {
                        if (!connectionMap.containsKey(message.getData()))
                        {
                            connectionMap.put(message.getData(), connection);
                            connection.send(new Message(MessageType.NAME_ACCEPTED));
                            return message.getData();
                        }
                    }
                }
            }
        }
        private void sendListOfUsers(Connection connection, String userName) throws IOException{
            for(Map.Entry<String,Connection> map : connectionMap.entrySet()){
                if(!map.getKey().equals(userName)){
                    connection.send(new Message(MessageType.USER_ADDED,map.getKey()));
                }
            }
        }
        private void serverMainLoop(Connection connection, String userName) throws IOException, ClassNotFoundException{
            while(true){
                Message userMessage = connection.receive();
                if(userMessage.getType() == MessageType.TEXT){
                    String text = userName + ": " + userMessage.getData();
                    sendBroadcastMessage(new Message(MessageType.TEXT, text));
                }else{
                    ConsoleHelper.writeMessage("Error in serverMainLoop");
                }
            }
        }
    }

    public static void main(String[] args) throws IOException
    {
        try(ServerSocket serverSocket = new ServerSocket(ConsoleHelper.readInt())){
            ConsoleHelper.writeMessage("Server is run!");
            while(true){
                Handler handler = new Handler(serverSocket.accept());
                handler.start();
            }
        }catch (IOException e){
            ConsoleHelper.writeMessage("Error server socket");
        }
    }
}
