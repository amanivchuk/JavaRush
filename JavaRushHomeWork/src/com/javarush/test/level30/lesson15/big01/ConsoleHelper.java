package com.javarush.test.level30.lesson15.big01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleHelper {
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    public static void writeMessage(String message){
        System.out.println(message);
    }
    public static String readString(){
        String str = null;
        try {
            writeMessage("Enter text!");
            str = reader.readLine();
            if(str == null || str.equals(" ")){
                System.out.println("Произошла ошибка при попытке ввода текста. Попробуйте еще раз.");
                str = readString();
            }
        }catch(IOException e){
            e.printStackTrace();
        }
        return str;
    }
    public static int readInt(){
        int digit = 0;
        try {
            try{
                writeMessage("Enter a digit");
                digit = Integer.parseInt(readString());
            }catch (NumberFormatException e){
                System.out.println("Произошла ошибка\n" +
                        "при попытке ввода числа. Попробуйте еще раз.");
                digit = readInt();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return digit;
    }
}
