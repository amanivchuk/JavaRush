package com.javarush.test.level22.lesson13.task02;

import java.io.*;
import java.util.ArrayList;

/* Смена кодировки
В метод main первым параметром приходит имя файла, тело которого в кодировке Windows-1251.
В метод main вторым параметром приходит имя файла, в который необходимо записать содержимое первого файла в кодировке UTF-8.
*/
public class Solution {
    static String win1251TestString = "РќР°СЂСѓС€РµРЅРёРµ РєРѕРґРёСЂРѕРІРєРё РєРѕРЅСЃРѕР»Рё?"; //only for your testing

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(args[0]));
        ArrayList<String> lines = new ArrayList<>();
        while(reader.ready()){
            lines.add(reader.readLine());
        }
        reader.close();
        ArrayList<String> decodedLines = new ArrayList<>();
        for(String l : lines){
            decodedLines.add(new String(l.getBytes("Windows-1251")));
        }
        PrintWriter printWriter = new PrintWriter(new FileWriter(args[1]));
        for(String currentLines : decodedLines){
            printWriter.println(currentLines);
        }
        printWriter.close();

    }
}
