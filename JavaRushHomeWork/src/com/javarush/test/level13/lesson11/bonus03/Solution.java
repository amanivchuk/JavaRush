package com.javarush.test.level13.lesson11.bonus03;

/* ����� �������
1 ����������� � ���, ��� ��� ��������.
2 http://info.javarush.ru/uploads/images/00/00/07/2014/08/12/50f3e37f94.png
3 ��������� �� ����� ������� � ���������� ����������� � ����.
4 ...
5 ������������, ��� �� ��� ������.

6 �������� ��� �������� ����� ����������� � �������� ����� ������:
6.1 ������� ����� AbstractRobot �����������, ������� ������ ����� � ������ �� Robot � AbstractRobot.
6.2 ��������������� ����� Robot �������� AbstractRobot.
6.3 ��������� ����� BodyPart ����� ������ ���� "�����".
6.4 �������� ����� ����� ���� � ���������� ����������� Attackable � Defensable (� ������ AbstractRobot).

7 http://info.javarush.ru/uploads/images/00/00/07/2014/08/12/3b9c65580b.png
*/

public class Solution
{
    public static void main(String[] args)
    {
        Robot amigo = new Robot("�����");
        Robot enemy = new Robot("����������-02");

        doMove(amigo, enemy);
        doMove(amigo, enemy);
        doMove(enemy, amigo);
        doMove(amigo, enemy);
        doMove(enemy, amigo);
        doMove(amigo, enemy);
        doMove(enemy, amigo);
        doMove(amigo, enemy);
    }

    public static void doMove(AbstractRobot robotFirst, AbstractRobot robotSecond) {
        BodyPart attacked = robotFirst.attack();
        BodyPart defenced = robotFirst.defense();
        System.out.println(String.format("%s �������� ������ %s, ��������� %s, �������� %s",
                robotFirst.getName(), robotSecond.getName(), attacked, defenced));
    }
}
