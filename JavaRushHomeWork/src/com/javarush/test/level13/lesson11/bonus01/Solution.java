package com.javarush.test.level13.lesson11.bonus01;

/* Сортировка четных чисел из файла
1. Ввести имя файла с консоли.
2. Прочитать из него набор чисел.
3. Вывести на консоль только четные, отсортированные по возрастанию.
Пример ввода:
5
8
11
3
2
10
Пример вывода:
2
8
10
*/

import java.io.*;
import java.util.*;

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        //BufferedInputStream in = new BufferedInputStream(new FileInputStream(reader.readLine()));
        BufferedReader in = new BufferedReader(new FileReader(reader.readLine()));
        List<Integer> list  = new ArrayList<Integer>();
        while(in.ready())
        {
            int data = Integer.parseInt(in.readLine());
            list.add(data);
        }
        Collections.sort(list);
        for(Integer i : list){
            if((i % 2) == 0){
                System.out.print(i + " ");
            }
        }
        reader.close();
        in.close();
    }
}

