package com.javarush.test.level21.lesson16.big01;

import java.util.ArrayList;

public class Hippodrome
{
    private ArrayList<Horse> horses = new ArrayList<>();
    public static Hippodrome game;

    public static void main(String[] args)
    {
        game = new Hippodrome();
        Horse horseA = new Horse("AHor", 3, 0);
        Horse horseB = new Horse("BHor", 3, 0);
        Horse horseC = new Horse("CHor", 3, 0);
        game.getHorses().add(horseA);
        game.getHorses().add(horseB);
        game.getHorses().add(horseC);
        try {
            game.run();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        game.printWinner();
    }
    public ArrayList<Horse> getHorses()
    {
        return horses;
    }
    public void run() throws InterruptedException {
        for (int i = 0; i < 100; i++){
            move();
            print();
            Thread.sleep(200);
        }
    }
    public void move(){
        for(Horse h : horses){
            h.move();
        }
    }
    public void print(){
        for(Horse h : horses){
            h.print();
        }
        System.out.println();
        System.out.println();
    }
    public Horse getWinner(){
        Horse winner = horses.get(0);
        for(Horse h : horses){
            if(h.getDistance() > winner.getDistance())
                winner = h;
        }
        return winner;
    }
    public void printWinner(){
        System.out.println("Winner is " + getWinner().getName() + "!");
    }

}
