package com.javarush.test.level18.lesson05.task03;

/* Разделение файла
Считать с консоли три имени файла: файл1, файл2, файл3.
Разделить файл1 по следующему критерию:
Первую половину байт записать в файл2, вторую половину байт записать в файл3.
Если в файл1 количество байт нечетное, то файл2 должен содержать бОльшую часть.
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file1 = reader.readLine();
        String file2 = reader.readLine();
        String file3 = reader.readLine();

        InputStream inputStream1 = new FileInputStream(file1);
        FileOutputStream out2 = new FileOutputStream(file2);
        FileOutputStream out3 = new FileOutputStream(file3);

        while (inputStream1.available() > 0){
            if(inputStream1.available() % 2 == 0){
                byte[] first = new byte[inputStream1.available() / 2];
                byte[] second = new byte[inputStream1.available() /2];
                int count1 = inputStream1.read(first);
                int count2 = inputStream1.read(second);
                out2.write(first,0,count1);
                out3.write(second,0,count2);
            } else{
                byte[] firstHalf = new byte[inputStream1.available() / 2 + 1];
                byte[] secondHalf = new byte[inputStream1.available() / 2];
                int count1 = inputStream1.read(firstHalf);
                int count2 = inputStream1.read(secondHalf);
                out2.write(firstHalf,0,count1);
                out3.write(secondHalf,0,count2);
            }
        }
        reader.close();
        inputStream1.close();
        out2.close();
        out3.close();
    }
}
