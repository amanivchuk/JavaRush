package com.javarush.test.level18.lesson05.task04;

/* Реверс файла
Считать с консоли 2 имени файла: файл1, файл2.
Записать в файл2 все байты из файл1, но в обратном порядке
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = reader.readLine();
        String fileName2 = reader.readLine();

        InputStream file1 = new FileInputStream(fileName1);
        OutputStream file2 = new FileOutputStream(fileName2);

        byte[] buff = new byte[file1.available()];
        file1.read(buff);
        //while((file1.read(buff)) != -1){
            for(int i = 0, j = buff.length-1; i < j; i++, j--){
                byte t = buff[i];
                buff[i] = buff[j];
                buff[j] = t;
            }
            file2.write(buff);
        //}
        file1.close();
        file2.close();
        reader.close();
    }
}
