package com.javarush.test.level18.lesson03.task05;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Set;
import java.util.TreeSet;




/* Сортировка байт
Ввести с консоли имя файла
Считать все байты из файла.
Не учитывая повторений - отсортировать их по байт-коду в возрастающем порядке.
Вывести на экран
Закрыть поток ввода-вывода

Пример байт входного файла
44 83 44

Пример вывода
44 83
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file = reader.readLine();
        Set<Integer> set = new TreeSet<Integer>();
        InputStream inputStream = new FileInputStream(file);
        try {
            while(inputStream.available() > 0){
                set.add(inputStream.read());
            }
            for(Integer x : set){
                System.out.print(x + " ");
            }
        }finally {
            reader.close();
            inputStream.close();
        }
    }
}
