package com.javarush.test.level18.lesson03.task03;

import java.io.*;

/* Самые частые байты
Ввести с консоли имя файла
Найти байт или байты с максимальным количеством повторов
Вывести их на экран через пробел
Закрыть поток ввода-вывода
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file = reader.readLine();
        InputStream in = new BufferedInputStream(new FileInputStream(file));
        try{
            int[] array = new int[128];
            while(in.available() > 0){
                int readByte = in.read();
                array[readByte] +=1;
            }
            int maxCount = 0;
            int maxByte = 0;
            for(int i = 0; i < array.length;i++){
                if(array[i] > maxCount){
                    maxCount = array[i];
                    maxByte = i;
                }
            }
            System.out.println(maxByte + "    " + maxCount);
        }finally
        {
            in.close();
        }
    }
}
