package com.javarush.test.level18.lesson03.task01;

import java.io.*;

/* Максимальный байт
Ввести с консоли имя файла
Найти максимальный байт в файле, вывести его на экран.
Закрыть поток ввода-вывода
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();
        InputStream in = new BufferedInputStream(new FileInputStream(s));
        try{
        int val;
        int max = 0;
        while((val = in.read()) != -1){
            if(max < val)
                max = val;
        }
        System.out.println(max);
        }finally
        {
            in.close();
        }
    }
}
