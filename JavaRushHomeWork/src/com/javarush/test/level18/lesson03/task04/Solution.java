package com.javarush.test.level18.lesson03.task04;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

/* Самые редкие байты
Ввести с консоли имя файла
Найти байт или байты с минимальным количеством повторов
Вывести их на экран через пробел
Закрыть поток ввода-вывода
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file = reader.readLine();
        InputStream inputStream = new FileInputStream(file);
        try
        {
            int[] array = new int[128];
            while (inputStream.available() > 0)
            {
                int readByte = inputStream.read();
                array[readByte] += 1;
            }
            int minCount = 10000;
            int minByte = 10000;
            for (int i = 0; i < array.length; i++)
            {
                if (array[i] != 0)
                {
                    if (array[i] < minCount)
                    {
                        minCount = array[i];
                        minByte = i;
                    }
                }
            }
            for(int i = 0; i < array.length; i++)
            {
                if (array[i] == minCount) System.out.println(i + " ");
            }
        }
        finally
        {
            inputStream.close();
        }
    }
}
