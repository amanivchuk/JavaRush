package com.javarush.test.level18.lesson03.task02;

import java.io.*;

/* Минимальный байт
Ввести с консоли имя файла
Найти минимальный байт в файле, вывести его на экран.
Закрыть поток ввода-вывода
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();
        InputStream in = new BufferedInputStream(new FileInputStream(s));
        try
        {
            int min = 100000;
            int ch;
            while ((ch = in.read()) != -1)
            {
                if (ch < min)
                    min = ch;
            }
            System.out.println(min);
        }finally
        {
            in.close();
        }
    }
}
