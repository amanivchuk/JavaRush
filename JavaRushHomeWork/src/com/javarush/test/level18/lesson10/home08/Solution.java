package com.javarush.test.level18.lesson10.home08;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* Нити и байты
Читайте с консоли имена файлов, пока не будет введено слово "exit"
Передайте имя файла в нить ReadThread
Нить ReadThread должна найти байт, который встречается в файле максимальное число раз, и добавить его в словарь resultMap,
где параметр String - это имя файла, параметр Integer - это искомый байт.
Закрыть потоки. Не использовать try-with-resources
*/

public class Solution {
    public static Map<String, Integer> resultMap = new HashMap<String, Integer>();

    public static void main(String[] args) throws IOException
    {
        ArrayList<String> fileList = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();
        while(!(s.equals("exit"))) {
            fileList.add(s);
            s = reader.readLine();
        }
        reader.close();
        for(String e : fileList){
            new ReadThread(e).start();
        }

    }

    public static class ReadThread extends Thread {
        private final String fileName;
        FileInputStream in  = null;

        public ReadThread(String fileName) {
            this.fileName = fileName;
        }
        @Override
        public void run()
        {
            try
            {
                FileInputStream in  = new FileInputStream(fileName);
                ArrayList<Integer> list = new ArrayList<>();

                while(in.available() > 0){
                    list.add(in.read());
                }
                in.close();

                if(list.size() > 0){
                    int id = list.get(0);
                    int max = 1;
                    for(int i = 1; i < 257; i++){
                        int sum = 0;
                        for(int k : list){
                            if(k == i)
                                sum++;

                            if(sum > max){
                                id = i;
                                max = sum;
                            }
                        }
                        resultMap.put(fileName,id);
                    }
                }
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}
