package com.javarush.test.level18.lesson10.home04;

/* Объединение файлов
Считать с консоли 2 имени файла
В начало первого файла записать содержимое второго файла так, чтобы получилось объединение файлов
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String first_file = reader.readLine();
        String second_File = reader.readLine();
        ArrayList<Integer> list = new ArrayList<Integer>();
        FileInputStream file1 = new FileInputStream(first_file);
        FileInputStream file2 = new FileInputStream(second_File);

        while(file2.available() >0){
            list.add(file2.read());
        }
        while (file1.available() > 0){
            list.add(file1.read());
        }
        FileOutputStream out = new FileOutputStream(first_file);
        for(Integer symbol : list){
            out.write(symbol);
        }
        reader.close();
        file1.close();
        file2.close();
        out.close();

//        RandomAccessFile r1 = new RandomAccessFile(first_file, "rw");
//        RandomAccessFile r2 = new RandomAccessFile(second_File, "rw");
//
//        byte[] arr_second = new byte[(int)r2.length()];
//        r2.read(arr_second);
//
//        byte[] arr_first = new byte[(int)r1.length()];
//        r1.read(arr_first);
//
//        r1.seek(0);
//        r1.write(arr_second);
//        r1.write(arr_first);
//
//        r1.close();
//        r2.close();
//        reader.close();
    }
}
