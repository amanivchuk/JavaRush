package com.javarush.test.level18.lesson10.home02;

/* Пробелы
В метод main первым параметром приходит имя файла.
Вывести на экран соотношение количества пробелов к количеству всех символов. Например, 10.45
1. Посчитать количество всех символов.
2. Посчитать количество пробелов.
3. Вывести на экран п2/п1*100, округлив до 2 знаков после запятой
4. Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        InputStream in = new FileInputStream(new File(args[0]));
        int symbol = 0;
        int probel = 0;
        while(in.available() > 0){
            int currentByte = in.read();
            symbol++;
            if(currentByte == (int)(' ')){
                probel++;
            }
        }
        in.close();
        double res = (double)probel / symbol * 100;
        System.out.printf("%.2f", res);

    }
}
