package com.javarush.test.level18.lesson10.home05;

/* Округление чисел
Считать с консоли 2 имени файла
Первый файл содержит вещественные(дробные) числа, разделенные пробелом. Например, 3.1415
Округлить числа до целых и записать через пробел во второй файл
Закрыть потоки. Не использовать try-with-resources
Принцип округления:
3.49 - 3
3.50 - 4
3.51 - 4
-3.49 - -3
-3.50 - -3
-3.51 - -4
*/

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file_1 = reader.readLine();
        String file_2 = reader.readLine();
        ArrayList<Integer> list = new ArrayList<Integer>();
        BufferedReader file1 = new BufferedReader(new FileReader(new File(file_1)));
        FileOutputStream outputStream = new FileOutputStream(new File(file_2));

        reader.close();
        String[] text = file1.readLine().split(" ");
        for(String i : text){
            System.out.println(i);
            list.add((int)Math.round(Double.parseDouble(i)));
        }
        for(Integer i : list){
            outputStream.write((i.toString() + " ").getBytes());
        }
        file1.close();
        outputStream.close();
    }
}
