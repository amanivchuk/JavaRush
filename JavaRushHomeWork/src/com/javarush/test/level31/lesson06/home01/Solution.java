package com.javarush.test.level31.lesson06.home01;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/* Добавление файла в архив
В метод main приходит список аргументов.
Первый аргумент - полный путь к файлу fileName.
Второй аргумент - путь к zip-архиву.
Добавить файл (fileName) внутрь архива в директорию 'new'.
Если в архиве есть файл с таким именем, то заменить его.

Пример входных данных:
C:/result.mp3
C:/pathToTest/test.zip

Файлы внутри test.zip:
a.txt
b.txt

После запуска Solution.main архив test.zip должен иметь такое содержимое:
new/result.mp3
a.txt
b.txt

Подсказка: нужно сначала куда-то сохранить содержимое всех энтри,
а потом записать в архив все энтри вместе с добавленным файлом.
Пользоваться файловой системой нельзя.
*/
public class Solution{
    public static Map<ZipEntry, byte[]> entryMap = new HashMap<>();

    public static void main(String[] args) throws FileNotFoundException
    {
        File zipArchive = new File(args[1]);
        File addFile = new File(args[0]);

        zipToMap(zipArchive);
        addNewFileToZip(addFile, zipArchive);
    }

    private static void addNewFileToZip(File addFile, File zipArchive)
    {
        try(ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(zipArchive));
        FileInputStream fis  = new FileInputStream(addFile))
        {
            //Маркер наличия добавляемого файла в архив
            boolean isExist = false;

            //Сохраняем сразу в отдельный ZipEntry добавляемый файл для последующего сравнения
            ZipEntry fileNameComparator = new ZipEntry(addFile.getName());

            //Копируем zipEntry c EntryMap в архив
            for(Map.Entry<ZipEntry,byte[]> zipEntry : entryMap.entrySet()){
                //Подрезаем путь файла для сравнения с добавляемым файлом
                Path path = Paths.get(zipEntry.getKey().getName());

                //Сравниваем
                //если имя текущего файла не совпадает с добавляемым файлом
                if(!((path.getFileName().toString().equals(fileNameComparator.getName())))){
                    zipOutputStream.putNextEntry(new ZipEntry(zipEntry.getKey().getName()));
                    zipOutputStream.write(zipEntry.getValue());
                }else {
                    isExist = true;
                }
            }
            //Если в процессе прохождения цикла нашелся такой же файл
            if(isExist){
                //Доюавляем файл в папку new
                ZipEntry addingFileEntry = new ZipEntry("/new" + addFile.getName());
                zipOutputStream.putNextEntry(addingFileEntry);

                //считываем содержимое файла в массив байт
                byte[] buffer = new byte[fis.available()];
                fis.read(buffer);

                //Добавляем содержимое к архиву
                zipOutputStream.write(buffer);
                //Закрываем текущую запись для новой записи
                zipOutputStream.closeEntry();
            }
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private static void zipToMap(File file) throws FileNotFoundException
    {
        try(ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(file)))
        {
            ZipEntry zipEntry;

            //перебираем все zipEntries
            while((zipEntry = zipInputStream.getNextEntry()) != null){
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

                byte[] buffer = new byte[1024];
                int count;

                while((count = zipInputStream.read(buffer)) != -1){
                    byteArrayOutputStream.write(buffer, 0 ,count);
                }
                byte[] bytes = byteArrayOutputStream.toByteArray();
                entryMap.put(zipEntry, bytes);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }


}










/*
public class Solution {
    public static void main(String[] args) throws IOException {
        String filePath = args[0];
        String zipFilePath = args[1];

        //String filePath = "src/main/resources/level31/lesson06/home01/test.txt";
        //String zipFilePath = "src/main/resources/level31/lesson06/home01/1.zip";
        //createTestZipFile(zipFilePath);

        // get simple file name (without path)
        String fileName = new File(filePath).getName();

        ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(zipFilePath));
        Map<ZipEntry, byte[]> entryMap = new HashMap<>();
        ZipEntry entry;
        while ((entry = zipInputStream.getNextEntry()) != null) {
            ByteArrayOutputStream byteArrayOutStream = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int count;
            while ((count = zipInputStream.read(buf)) != -1) {
                byteArrayOutStream.write(buf, 0, count);
            }
            entryMap.put(entry, byteArrayOutStream.toByteArray());
        }
        zipInputStream.close();


        // copy entries to the same zipFile
        ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(zipFilePath));
        for (Map.Entry<ZipEntry, byte[]> zipEntry : entryMap.entrySet()) {
            // skip file with the same name as 'fileName'
            if (fileName.equals(zipEntry.getKey().getName())) {
                continue;
            }
            zipOut.putNextEntry(zipEntry.getKey());
            zipOut.write(zipEntry.getValue());
            zipOut.closeEntry();
        }

        FileInputStream fis = new FileInputStream(filePath);
        byte[] fileBytes = new byte[fis.available()];
        fis.read(fileBytes);
        fis.close();

        // add our file at the end
        ZipEntry zipEntry = new ZipEntry("new/" + fileName);
        zipOut.putNextEntry(zipEntry);
        zipOut.write(fileBytes);
        zipOut.closeEntry();
        zipOut.close();
    }

   */
/* // create test .zip file
    private static void createTestZipFile(String zipFilePath) throws IOException {
        ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(zipFilePath));

        zipOut.putNextEntry(new ZipEntry("a.txt"));
        zipOut.write("aaa".getBytes());
        zipOut.closeEntry();

        zipOut.putNextEntry(new ZipEntry("b.txt"));
        zipOut.write("bbb".getBytes());
        zipOut.closeEntry();

        zipOut.putNextEntry(new ZipEntry("test.txt"));
        zipOut.write("afa".getBytes());
        zipOut.closeEntry();

        zipOut.close();
    }*//*

}*/
