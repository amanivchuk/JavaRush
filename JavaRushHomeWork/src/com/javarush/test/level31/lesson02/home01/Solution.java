package com.javarush.test.level31.lesson02.home01;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/* Проход по дереву файлов
1. На вход метода main подаются два параметра.
Первый - path - путь к директории, второй - resultFileAbsolutePath - имя файла, который будет содержать результат.
2. Для каждого файла в директории path и в ее всех вложенных поддиректориях выполнить следующее:
2.1. Если у файла длина в байтах больше 50, то удалить его.
2.2. Если у файла длина в байтах НЕ больше 50, то для всех таких файлов:
2.2.1. отсортировать их по имени файла в возрастающем порядке, путь не учитывать при сортировке
2.2.2. переименовать resultFileAbsolutePath в 'allFilesContent.txt'
2.2.3. в allFilesContent.txt последовательно записать содержимое всех файлов из п. 2.2.1. Тела файлов разделять "\n"
2.3. Удалить директории без файлов (пустые).
Все файлы имеют расширение txt.
*/
public class Solution {
    public static void main(String[] args) throws IOException
    {
        String path = args[0];
        String resultFileAbsolutePath = args[1];
        String allFilesContent = "allFilesContent.txt";

        saveFileLessThan50Bytes(path);
        //rename file
        Path source = Paths.get(resultFileAbsolutePath);
        Path newResPath = Files.move(source,source.resolveSibling(allFilesContent));
        //delete resultFileAbsolutePath if list contains it
        lessThan50BytesFile.remove(new File(resultFileAbsolutePath));
        //sort
        Collections.sort(lessThan50BytesFile, new Comparator<File>()
        {
            @Override
            public int compare(File o1, File o2)
            {
                String fileName1 = o1.getName();
                String fileName2 = o2.getName();
                return fileName1.compareTo(fileName2);
            }
        });
        //write result
        BufferedWriter fout = new BufferedWriter(new FileWriter(newResPath.toFile()));
        for(File file : lessThan50BytesFile){
            BufferedReader fin = new BufferedReader(new FileReader(file));
            while(fin.ready()){
                fout.write(fin.readLine());
                fout.newLine();
            }
            fin.close();
        }
        fout.close();
    }

    private static ArrayList<File> lessThan50BytesFile = new ArrayList<>();
    private static void saveFileLessThan50Bytes(String path)
    {
        File dir = new File(path);
        File[] files = dir.listFiles();

        if(files == null)
            return;
        else if(files.length == 0){
            dir.delete();
        }
        else{
            for(File file : files){
                if(file.isDirectory()){
                    saveFileLessThan50Bytes(file.getAbsolutePath());
                } else{
                    if(file.length() > 50){
                        file.delete();
                    }else{
                        lessThan50BytesFile.add(file);
                    }
                }
            }
        }
    }
}
