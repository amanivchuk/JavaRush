package com.javarush.test.level04.lesson16.home02;

import java.io.*;

/* Среднее такое среднее
Ввести с клавиатуры три числа, вывести на экран среднее из них. Т.е. не самое большое и не самое маленькое.
*/

public class Solution
{
    public static void main(String[] args)   throws Exception
    {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String a = reader.readLine();
        String b = reader.readLine();
        String c = reader.readLine();
        int i = Integer.parseInt(a);
        int j = Integer.parseInt(b);
        int k = Integer.parseInt(c);
        if ((i>j&&i<k)||(i>k&&i<j))
            System.out.println(i);
        else
        if ((j>i&&j<k)||(j>k&&j<i))
            System.out.println(j);
        else
            System.out.println(k);

    }
}
