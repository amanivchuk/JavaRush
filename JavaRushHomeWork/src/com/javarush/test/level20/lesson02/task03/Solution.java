package com.javarush.test.level20.lesson02.task03;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/* Знакомство с properties
В методе fillInPropertiesMap считайте имя файла с консоли и заполните карту properties данными из файла.
Про .properties почитать тут - http://ru.wikipedia.org/wiki/.properties
Реализуйте логику записи в файл и чтения из файла для карты properties.
*/
public class Solution {
    public static Map<String, String> properties = new HashMap<>();
    Properties prop = new Properties();
    public static void main(String[] args) throws Exception
    {
        new Solution().fillInPropertiesMap();
        System.out.println(properties);
        new Solution().save(new FileOutputStream("E:/2.properties"));
    }
    public void fillInPropertiesMap() throws Exception
    {
        //implement this method - реализуйте этот метод
        BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
        InputStream reader = new FileInputStream(read.readLine());
        load(reader);
        read.close();
    }

    public void save(OutputStream outputStream) throws Exception {
        //implement this method - реализуйте этот метод
        PrintWriter writer = new PrintWriter(outputStream);
        if(prop.size() > 0){
            prop.putAll(properties);
        }
        prop.store(outputStream, "");
        writer.close();
    }

    public void load(InputStream inputStream) throws Exception {
        //implement this method - реализуйте этот метод
        BufferedReader reader  = new BufferedReader(new InputStreamReader(inputStream));
        prop.load(inputStream);
        Set<String> list = prop.stringPropertyNames();
        for(String s : list){
            properties.put(s, prop.getProperty(s));
            reader.close();
        }
    }
}
