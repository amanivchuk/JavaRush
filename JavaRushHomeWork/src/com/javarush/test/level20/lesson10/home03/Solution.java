package com.javarush.test.level20.lesson10.home03;

import java.io.*;

/* Найти ошибки
Почему-то при сериализации/десериализации объекта класса B возникают ошибки.
Найдите проблему и исправьте ее.
Класс A не должен реализовывать интерфейсы Serializable и Externalizable.
Сигнатура класса В не содержит ошибку :)
Метод main не участвует в тестировании.
*/
public class Solution implements Serializable{
    static final long serialVersionID = 12L;
//    public static void main(String[] args) throws IOException, ClassNotFoundException
//    {
//        ByteArrayOutputStream out = new ByteArrayOutputStream();
//        ObjectOutputStream objectOut = new ObjectOutputStream(out);
//        A a = new A("A");
//        Solution.B b = new Solution().new B("B");
//        objectOut.writeObject(b);
//        objectOut.flush();
//        objectOut.close();
//
//        ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
//        ObjectInputStream objectIn = new ObjectInputStream(in);
//        B b1 = (B) objectIn.readObject();
//        System.out.println(b1.name);
//        objectIn.close();
//
//    }
    public static class A   {
        static final long serialVersionID = 12L;
        protected String name = "A";
        public A(){}
        public A(String name) {
            this.name += name;
        }
    }

    public class B extends A implements Serializable {
        static final long serialVersionID = 12L;
        public B(String name) {
            super(name);
            this.name += name;
        }
        private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
            name = (String) s.readObject();
        }
        private void writeObject(ObjectOutputStream s) throws IOException {
            s.writeObject(name);
        }

    }
}
