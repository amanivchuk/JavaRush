package com.javarush.test.level32.lesson15.big01;

import com.javarush.test.level32.lesson15.big01.listeners.FrameListener;
import com.javarush.test.level32.lesson15.big01.listeners.TabbedPaneChangeListener;
import com.javarush.test.level32.lesson15.big01.listeners.UndoListener;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.undo.UndoManager;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class View extends JFrame implements ActionListener {
    private Controller controller;
    private JTabbedPane tabbedPane = new JTabbedPane();
    private JTextPane htmlTextPane = new JTextPane();
    private JEditorPane plainTextPane = new JEditorPane();
    private UndoManager undoManager = new UndoManager();
    private UndoListener undoListener = new UndoListener(undoManager);

    public View(){
        try { UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());}
        catch (ClassNotFoundException e) { ExceptionHandler.log(e); }
        catch (InstantiationException e) { ExceptionHandler.log(e); }
        catch (IllegalAccessException e) { ExceptionHandler.log(e); }
        catch (UnsupportedLookAndFeelException e) { ExceptionHandler.log(e);}
    }

    public Controller getController() { return controller; }

    public void setController(Controller controller) { this.controller = controller; }

    public UndoListener getUndoListener() { return undoListener; }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command){
            case "Новый":
                controller.createNewDocument();
                break;
            case "Открыть":
                controller.openDocument();
                break;
            case "Сохранить":
                controller.saveDocument();
                break;
            case "Сохранить как...":
                controller.saveDocumentAs();
                break;
            case "Выход":
                controller.exit();
                break;
            case "О программе":
                showAbout();
                break;
        }
    }
    public void init(){
        this.initGui();
        FrameListener frameListener = new FrameListener(this);
        addWindowListener(frameListener);
        setVisible(true);
    }

    public void initMenuBar(){
        JMenuBar jMenuBar = new JMenuBar();
        MenuHelper.initFileMenu(this, jMenuBar);
        MenuHelper.initEditMenu(this, jMenuBar);
        MenuHelper.initStyleMenu(this, jMenuBar);
        MenuHelper.initAlignMenu(this, jMenuBar);
        MenuHelper.initColorMenu(this, jMenuBar);
        MenuHelper.initFontMenu(this, jMenuBar);
        MenuHelper.initHelpMenu(this, jMenuBar);
        getContentPane().add(jMenuBar, BorderLayout.NORTH);
    }
    public void initEditor(){
        htmlTextPane.setContentType("text/html");
        JScrollPane jScrollPane = new JScrollPane(htmlTextPane);
        tabbedPane.add("HTML", jScrollPane);
        JScrollPane jScrollPane1 = new JScrollPane(plainTextPane);
        tabbedPane.add("Текст", jScrollPane1);
        tabbedPane.setPreferredSize(new Dimension(150, 150));
        tabbedPane.addChangeListener(new TabbedPaneChangeListener(this));
        Container contentPaint = getContentPane();
        contentPaint.add(tabbedPane, BorderLayout.CENTER);

    }
    public void initGui(){
        this.initMenuBar();
        this.initEditor();
        pack();
    }

    public void exit(){
        controller.exit();
    }

    public boolean canRedo() {
        return undoManager.canRedo();
    }

    public boolean canUndo() {
        return undoManager.canUndo();
    }

    public void undo() {
        try { undoManager.undo(); }
        catch (Exception e) { ExceptionHandler.log(e); }
    }

    public void redo() {
        try { undoManager.redo(); }
        catch (Exception e) { ExceptionHandler.log(e); }
    }

    public void resetUndo() {
        undoManager.discardAllEdits();
    }
    public boolean isHtmlTabSelected(){
        return tabbedPane.getSelectedIndex() == 0;
    }
    public void selectHtmlTab(){
        tabbedPane.setSelectedIndex(0);
        resetUndo();
    }
    public void update(){
        htmlTextPane.setDocument(controller.getDocument());
    }
    public void showAbout(){
        JOptionPane.showMessageDialog(getContentPane(), "All right reserved.", "About program", JOptionPane.INFORMATION_MESSAGE);
    }
    public void selectedTabChanged(){
        if (isHtmlTabSelected()) controller.setPlainText(plainTextPane.getText());
        else plainTextPane.setText(controller.getPlainText());
        resetUndo();
    }
}
