package com.javarush.test.level17.lesson10.home03;

import java.util.ArrayList;
import java.util.List;

/* ������
�������� ��������� Runnable � ������� Apteka � Person.
��� ���� ������ �������� ���� �� isStopped
������ ��� Apteka: drugsController ������ ������� ������� ���������� ��������� (getRandomDrug) � ���������� (getRandomCount) � ��������� 300 ��
������ ��� Person: drugsController ������ ������� ������� ���������� ��������� (getRandomDrug) � ���������� (getRandomCount) � ��������� 100 ��
�������� synchronized ���, ��� ��� ����������
*/

public class Solution {
    public static DrugsController drugsController = new DrugsController();
    public static boolean isStopped = false;

    public static void main(String[] args) throws InterruptedException {
        Thread apteka = new Thread(new Apteka());
        Thread man = new Thread(new Person(), "�������");
        Thread woman = new Thread(new Person(), "�������");

        apteka.start();
        man.start();
        woman.start();

        Thread.sleep(1000);
        isStopped = true;
    }

    public static class Apteka implements Runnable {

        @Override
        public void run()
        {
            while(!isStopped){
                drugsController.buy(getRandomDrug(),getRandomCount());
                try
                {
                    Thread.sleep(300);
                }
                catch (InterruptedException e)
                {
                    /* NOP */
                }
            }
        }
    }

    public static class Person implements Runnable {

        @Override
        public void run()
        {
            while (!isStopped){
                drugsController.sell(getRandomDrug(),getRandomCount());
               waitAMoment();
            }
        }
    }

    public static int getRandomCount() {
        return (int) (Math.random() * 3) + 1;
    }

    public static Drug getRandomDrug() {
        int index = (int) ((Math.random() * 1000) % (drugsController.allDrugs.size()));
        List<Drug> drugs = new ArrayList<Drug>(drugsController.allDrugs.keySet());
        return drugs.get(index);
    }

    private static void waitAMoment() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
        }
    }
}
