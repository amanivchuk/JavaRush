package com.javarush.test.level14.lesson08.bonus02;

/* НОД
Наибольший общий делитель (НОД).
Ввести с клавиатуры 2 целых положительных числа.
Вывести в консоль наибольший общий делитель.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int i = Integer.parseInt(reader.readLine());
        int j = Integer.parseInt(reader.readLine());

        System.out.println(nod(i, j));

    }

    private static int nod(int i, int j)
    {
        int min;
        int nod = 0;
        if(i > j)
            min = j;
        else min = i;
        for(int k = 1; k <=min; k++){
            if((i % k == 0) && (j % k == 0)) {
                if(k > nod)
                    nod = k;
            }
        }
        return nod;
    }
}
