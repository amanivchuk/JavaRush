package com.javarush.test.level14.lesson06.home01;

/**
 * Created by ASUS on 02.09.2015.
 */

public class UkrainianHen extends Hen
{
    @Override
    int getCountOfEggsPerMonth()
    {
        return 23456788;
    }

    @Override
    String getDescription()
    {
        return super.getDescription() +" Моя страна - "+ Country.UKRAINE +". Я несу "+ getCountOfEggsPerMonth() +" яиц в месяц.";
    }
}