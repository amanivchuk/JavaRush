package com.javarush.test.level14.lesson06.home01;

/**
 * Created by ASUS on 02.09.2015.
 */

public class RussianHen extends Hen
{
    @Override
    int getCountOfEggsPerMonth()
    {
        return 35456789;
    }

    @Override
    String getDescription()
    {
        return super.getDescription() +" Моя страна - "+ Country.RUSSIA +". Я несу "+ getCountOfEggsPerMonth() +" яиц в месяц.";
    }
}