package com.javarush.test.level14.lesson06.home01;

/**
 * Created by ASUS on 02.09.2015.
 */
public class BelarusianHen extends Hen
{
    @Override
    int getCountOfEggsPerMonth()
    {
        return 79467387;
    }

    @Override
    String getDescription()
    {
        return super.getDescription() +" Моя страна - "+ Country.BELARUS +". Я несу "+ getCountOfEggsPerMonth() +" яиц в месяц.";
    }
}