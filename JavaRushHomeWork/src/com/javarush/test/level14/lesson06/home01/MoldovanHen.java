package com.javarush.test.level14.lesson06.home01;

/**
 * Created by ASUS on 02.09.2015.
 */
public class MoldovanHen extends Hen
{
    @Override
    int getCountOfEggsPerMonth()
    {
        return 12345678;
    }

    @Override
    String getDescription()
    {
        return super.getDescription() +" Моя страна - "+ Country.MOLDOVA +". Я несу "+ getCountOfEggsPerMonth() +" яиц в месяц.";
    }
}