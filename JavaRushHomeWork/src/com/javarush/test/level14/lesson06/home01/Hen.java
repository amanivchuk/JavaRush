package com.javarush.test.level14.lesson06.home01;

/**
 * Created by ASUS on 02.09.2015.
 */
public abstract class Hen
{
    abstract int getCountOfEggsPerMonth();

    String getDescription(){
        return "Я курица.";
    }
}