package com.javarush.test.level24.lesson02.home01;

/**
 * Created by ASUS on 08.01.2016.
 */
public class SelfInterfaceMarkerImpl implements SelfInterfaceMarker
{
    public SelfInterfaceMarkerImpl()
    {
    }

    public void printX() {
        System.out.println("Oooo!");
    }
    public void printY(){
        System.out.println("Vauu!");
    }
}
