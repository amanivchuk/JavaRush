package com.javarush.test.level33.lesson15.big01;

import com.javarush.test.level33.lesson15.big01.strategies.HashMapStorageStrategy;
import com.javarush.test.level33.lesson15.big01.strategies.StorageStrategy;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Solution {
    public static Set<Long> getIds(Shortener shortener, Set<String> strings){
        Set<Long> set  = new HashSet<>();
        for(String string  : strings){
            long id = shortener.getId(string);
            set.add(id);
        }
        return set;
    }
    public static Set<String> getStrings(Shortener shortener, Set<Long> keys){
        Set<String> set = new HashSet<>();
        for(Long key : keys){
            set.add(shortener.getString(key));
        }
        return set;
    }
    public static void testStrategy(StorageStrategy strategy, long elementsNumber){
        String nameStrategy = strategy.getClass().getSimpleName();
        Helper.printMessage(nameStrategy);

        Set<String> strings = new HashSet<>();
        for(long l = 0; l < elementsNumber; l++){
            strings.add(Helper.generateRandomString());
        }

        Shortener shortener = new Shortener(strategy);
        Long time1 = new Date().getTime();
        Set<Long> keys = getIds(shortener, strings);
        Long time2 = new Date().getTime();
        Long duration  = time2 - time1;
        Helper.printMessage(duration.toString());

        Long time3 = new Date().getTime();
        Set<String> stringSet = getStrings(shortener, keys);
        Long time4 = new Date().getTime();
        Long duration2 = time4 - time3;
        Helper.printMessage(duration2.toString());

        if(stringSet.equals(strings)) Helper.printMessage("Тест пройден.");
        else Helper.printMessage("Тест не пройден.");

    }
    public static void main(String[] args){
        HashMapStorageStrategy hh = new HashMapStorageStrategy();
        testStrategy(hh, 10000L);
    }
}
