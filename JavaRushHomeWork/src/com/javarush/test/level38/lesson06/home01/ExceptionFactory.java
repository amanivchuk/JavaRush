package com.javarush.test.level38.lesson06.home01;

public class ExceptionFactory {
    public static Throwable getException(Enum enumaration){
        if(enumaration != null){
            if(enumaration instanceof ExceptionApplicationMessage){
                return new Exception(enumaration.name().charAt(0) + enumaration.name().substring(1).toLowerCase().replace("_", " "));
            }
            else if (enumaration instanceof ExceptionDBMessage){
                return new RuntimeException(enumaration.name().charAt(0) + enumaration.name().substring(1).toLowerCase().replace("_", " "));
            }
            else if (enumaration instanceof ExceptionUserMessage){
                return new Error(enumaration.name().charAt(0) + enumaration.name().substring(1).toLowerCase().replace("_" , " "));
            }
        }
        return new IllegalArgumentException();
    }
}
