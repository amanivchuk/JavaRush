package com.manivchukRestaurant.kitchen;

import java.util.Arrays;

public enum Dish {
    Fish, Steak, Soup, Juice, Water;
    public static String allDishesToString(){
        StringBuilder build = new StringBuilder();
        Dish[] dishes = Dish.values();
        build.append(Arrays.toString(dishes.toString().split(" ")));
        return build.toString();
    }
}
